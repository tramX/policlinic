from django.conf.urls import include, url
from django.contrib import admin
from app_policlinic.views import CreateReceptionView

urlpatterns = [
    # Examples:
    # url(r'^$', 'policlinic.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', CreateReceptionView.as_view(), name='create_reception'),

]
