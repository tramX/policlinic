from django.shortcuts import render, redirect
from django.views.generic.edit import CreateView
from app_policlinic.forms import CreateReceptionForm

# Create your views here.


class CreateReceptionView(CreateView):
    template_name = 'app_policlinic/index.html'
    form_class = CreateReceptionForm

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.start_time_reception = \
            form.cleaned_data['start_time_reception'].replace(second=0)
        instance.end_time_reception = \
            form.cleaned_data['start_time_reception'].replace(second=0) \
                .replace(hour=form.cleaned_data['start_time_reception'].hour+1)
        instance.save()
        return redirect("/")
