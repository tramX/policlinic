# coding: utf-8
from django import forms
from app_policlinic.models import Reception
from datetimewidget.widgets import DateWidget, TimeWidget
import datetime


class CreateReceptionForm(forms.ModelForm):

    class Meta:
        model = Reception
        exclude = ['end_time_reception']

        widgets = {
            'date_reception': DateWidget(attrs={'id': "date_reception"},
            usel10n=True, bootstrap_version=3),
            'start_time_reception':
            TimeWidget(attrs={'id': "start_time_reception"},
            usel10n = True, bootstrap_version=3)
        }

    def clean(self):
        time_start = datetime.time(hour=9, minute=0, second=0,
                                   microsecond=0, tzinfo=None)
        time_end = datetime.time(hour=18, minute=0, second=0,
                                 microsecond=0, tzinfo=None)
        select_time_client = self.cleaned_data.get('start_time_reception')

        # Проверка выходных дней
        if (self.cleaned_data.get('date_reception').weekday() == 5 or
            self.cleaned_data.get('date_reception').weekday() == 6):
                raise forms.ValidationError('Выходной день')

        # Проверка на вхождение выбранного времени в приемные часы.
        if (select_time_client < time_start or select_time_client > time_end):
            raise forms.ValidationError('Выбраны не приемные часы!')

        # Проверка оставшегося времени до конца рабочего дня
        if (select_time_client.replace(select_time_client.hour+1)
                    .replace(second=0) > time_end):
            raise forms.ValidationError('Не достаточно времени '
                                        'для полноценного осмотра.')

        # Проверка занятости выброного времени другим пациентом.
        if (Reception.objects.filter(date_reception=
            self.cleaned_data.get('date_reception'),
            select_doctor=self.cleaned_data.get('select_doctor')).count() > 0):
            for reception in Reception.objects.filter(date_reception=self.cleaned_data.get('date_reception'),
                select_doctor=self.cleaned_data.get('select_doctor')):
                if (select_time_client >= reception.start_time_reception and
                    select_time_client <= reception.end_time_reception):
                    raise forms.ValidationError('Выбранное время уже занято')

        return self.cleaned_data
