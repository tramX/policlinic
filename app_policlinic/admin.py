from django.contrib import admin
from app_policlinic.models import *

# Register your models here.


class ReceptionAdmin(admin.ModelAdmin):
    list_filter = ['select_doctor']
    list_display = ['fio', 'select_doctor', 'date_reception',
                    'start_time_reception', 'end_time_reception']

admin.site.register(Doctor)
admin.site.register(Reception, ReceptionAdmin)
