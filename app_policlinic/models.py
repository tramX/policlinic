from django.db import models

# Create your models here.


class Doctor(models.Model):
    fio = models.CharField(verbose_name="Ф.И.О врача", max_length=255)
    specialty = models.CharField(verbose_name="Специальность", max_length=255)

    class Meta:
        verbose_name = "Врачи"
        db_table = 'doctor'

    def __str__(self):
        return self.specialty


class Reception(models.Model):
    fio = models.CharField(verbose_name="Ф.И.О пациента", max_length=255)
    select_doctor = models.ForeignKey(Doctor, verbose_name="Выбор врача")
    date_reception = models.DateField(verbose_name="Дата приема")
    start_time_reception = models.TimeField(verbose_name="Время начала приема")
    end_time_reception = models.TimeField(verbose_name="Время "
                                                       "окончания приема")

    class Meta:
        verbose_name = "Приемы"
        db_table = 'reception'

    def __str__(self):
        return self.fio
